package server

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"sync"

	"gitlab.com/sosiv/peabody/config"
	"gitlab.com/sosiv/peabody/pkg/encryptor"
)

type Data struct {
	Data []string `json:"data"`
}

// Our custom handler that holds a wait group used to block the shutdown
type handler struct {
	ctx context.Context
	cfg *config.Config
}

func NewHandler(ctx context.Context, cfg *config.Config) *handler {
	return &handler{
		ctx: ctx,
		cfg: cfg,
	}
}

func (h *handler) encryptStrings(w http.ResponseWriter, r *http.Request) {
	reqData := Data{}

	if r.Method != http.MethodPost {
		http.Error(w, "not found", http.StatusNotFound)
		return
	}

	// decode the request body into the struct. If there is an error,
	// respond to the client with the error message and a 400 status code.
	err := json.NewDecoder(r.Body).Decode(&reqData)
	if err != nil {
		log.Printf("unable to encode request body: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	r.Body.Close()

	var wg sync.WaitGroup

	numJobs := len(reqData.Data)
	jobs := make(chan string, numJobs)
	results := make(chan string, numJobs)

	encryptedStrings := make([]string, numJobs)
	for i := 0; i < h.cfg.WorkersCount; i++ {
		select {
		// If the context was cancelled, a SIGTERM was captured
		case <-h.ctx.Done():
			wg.Wait()
			http.Error(w,"something went wrong", http.StatusInternalServerError)
			return
		default:
			wg.Add(1)
			go encryptor.EncryptString(&wg, jobs, results)
		}
	}

	for i := 0; i < numJobs; i++ {
		jobs <- reqData.Data[i]
	}
	close(jobs)

	for i := 0; i < numJobs; i++ {
		encryptedStrings[i] = <-results
	}

	//wait for all goroutines
	wg.Wait()

	responseData := Data{
		Data: encryptedStrings,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	err = json.NewEncoder(w).Encode(responseData)
	if err != nil {
		http.Error(w,"something went wrong", http.StatusInternalServerError)
	}

}
