package server

import (
	"net/http"
)

func New(h *handler, port string) *http.Server {
	router := http.NewServeMux()
	router.HandleFunc("/api/v1/encryptor_scopes/", h.encryptStrings)

	return &http.Server{
		Addr:    ":" + port,
		Handler: router,
	}
}
