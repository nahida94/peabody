#### String encryptor RESTFUL service

This microservice will get a list of strings and will return them as string encrypted in SHA256.

The project layouts designed based this guideline:
[project-layout](https://github.com/golang-standards/project-layout)

## Usage

##### run project in local
move cdm/peabody folder <br />
```cd cmd/peabody```

build project <br />
```go build```

define required environment variables
```PORT=8082```

##### run in docker 
build an image from a Dockerfile <br />
```docker build -t peabody . ``` <br />

run docker container <br />
``` docker run  --env PORT=8082 -p 8082:8082 peabody```

### env variables
```PORT``` required <br/>
```WORKERS_COUNT``` default value is 4
