package encryptor

import (
	"crypto/sha256"
	"fmt"
	"sync"
)

func EncryptString(wg *sync.WaitGroup, jobs <-chan string, results chan<- string) {
	defer wg.Done()

	for value := range jobs {
		encoded := sha256.Sum256([]byte(value))
		results <- fmt.Sprintf("%x", encoded)
	}
}
